FROM d4science/gcore-distribution
RUN wget --no-check-certificate https://nexus.d4science.org/nexus/content/repositories/gcube-staging-gcore/org/gcube/informationsystem/is-registry-service/2.1.4-4.16.0-126945/is-registry-service-2.1.4-4.16.0-126945.gar && mv is-registry-service-2.1.4-4.16.0-126945.gar is-registry-service.gar && gcore-deploy-service is-registry-service.gar

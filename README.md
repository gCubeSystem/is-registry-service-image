# IS Registry Service Docker image

This repo contains the Dockerfile to build the image of the gCore IS Registry service

## Build the image

```shell
$ docker build -t is-registry-service .
```

## Test the image

```shell
$ docker container run --name is-registry is-registry-service 
```
